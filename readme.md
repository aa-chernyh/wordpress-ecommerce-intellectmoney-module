#Модуль оплаты платежной системы IntellectMoney для CMS Wordpress с модулем eCommerce

> **Внимание!** <br>
Данная версия актуальна на *9 июля 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557815#eCommerce_files.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557815#55781507c0ac95719f4379a0efc2c2978fbaea
