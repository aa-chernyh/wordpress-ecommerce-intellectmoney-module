<?php

require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/VATs.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/UserSettings.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/LanguageHelper.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/Customer.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/Order.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/Payment.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/Result.php');
require_once(WPSC_FILE_PATH . '/wpsc-merchants/IntellectMoneyCommon/Status.php');

//Это общие настройки. Они загружаются и в админке, и при выставлении счета
$nzshpcrt_gateways[$num] = array(
    'name' => __( 'IntellectMoney', 'wp-e-commerce' ),
    'api_version' => 2.0,
    'image' => WPSC_URL . '/wpsc-merchants/images/im.png',
    'class_name' => 'wpsc_merchant_intellectmoney',
    'has_recurring_billing' => false,
    'display_name' => __( 'IntellectMoney', 'wp-e-commerce' ),
    'wp_admin_cannot_cancel' => false,
    'requirements' => array(
        'php_version' => 5.6
    ),
    'form' => 'intellectMoneyAdminSettingsDisplay',
    'submit_function' => 'intellectMoneyAdminSettingsSave',
    'internalname' => 'wpsc_merchant_intellectmoney',
);

//Выводит поля ввода настроек в админке. Назначается в $nzshpcrt_gateways['form']
function intellectMoneyAdminSettingsDisplay() {
    return wpsc_merchant_intellectmoney::getAdminForm();
}

//Сохраняет поля ввода настроек в админке. Назначается в $nzshpcrt_gateways['submit_function']
function intellectMoneyAdminSettingsSave() {
    wpsc_merchant_intellectmoney::saveAdminForm();
}


class wpsc_merchant_intellectmoney extends wpsc_merchant {

    public function __construct( $purchase_id = null, $is_receiving = false ) {  
        parent::__construct($purchase_id, $is_receiving);
    }

    public function submit() {
        $userSettings = $this->loadUserSettings();
        $customer = \PaySystem\Customer::getInstance($this->cart_data['email_address'], $this->cart_data['billing_address']['first_name'] . " " . $this->cart_data['billing_address']['last_name'], $this->cart_data['billing_address']['phone']);
        $order = \PaySystem\Order::getInstance($this->getImParams(new WPSC_Purchase_Log($this->purchase_id))['invoiceId'], $this->purchase_id, NULL, $this->cart_data['total_price'], $this->cart_data['total_price'], NULL, $this->cart_data['store_currency'], NULL, NULL);
        
        //products
        $sumOfItemsPrice = 0;
        foreach ($this->cart_items as $product) {
            $order->addItem($product['price'], $product['quantity'], $product['name'], $userSettings->getTax());
            $sumOfItemsPrice += $product['price'] * $product['quantity'];
        }
        //delivery
        $deliveryPrice = $this->cart_data['total_price'] - $sumOfItemsPrice;
        if ($deliveryPrice != 0) {
            $order->addItem($deliveryPrice, 1, 'delivery', $userSettings->getDeliveryTax());
        }

        $payment = \PaySystem\Payment::getInstance($userSettings, $order, $customer);
        $this->set_purchase_processed_by_purchid($this->purchaseId);
        wpsc_empty_cart();
        echo $payment->generateForm(true, true);
        exit();
    }
    
    public function process_gateway_notification() {
        parent::__construct($_REQUEST['orderId']);

        $purchaseLogObject = new WPSC_Purchase_Log($_REQUEST['orderId']); 
        $userSettings = $this->loadUserSettings();
        $order = \PaySystem\Order::getInstance(NULL, $this->purchase_id, $this->cart_data['total_price'], $this->cart_data['total_price'], NULL, NULL, $this->cart_data['store_currency'], NULL, $this->getImParams($purchaseLogObject)['lastImStatus']);
        $result = \PaySystem\Result::getInstance($_REQUEST, $userSettings, $order, 'en');

        $response = $result->processingResponse();
        if ($response->changeStatusResult) {
            $this->saveImParams($purchaseLogObject, $_REQUEST['paymentStatus'], $_REQUEST['paymentId']);
            $purchaseLogObject->set('processed', $this->getCmsStatusIdByName($response->statusCMS))->save();
        }

        echo $result->getMessage();
        die;
    }
    
    private function saveImParams($purchaseLogObject, $lastImStatus = null, $invoiceId = null) {
        $params = $this->getImParams($purchaseLogObject);
        
        if(!empty($lastImStatus)) {
            $params['lastImStatus'] = $lastImStatus;
        }
        if(!empty($invoiceId)) {
            $params['invoiceId'] = $invoiceId;
        }
        
        $purchaseLogObject->set('transactid', json_encode($params))->save();
    }
    
    private function getImParams($purchaseLogObject) {
        $notes = $purchaseLogObject->get_data()['transactid'];
        
        if(!empty($notes)) {
            return (array)json_decode($notes);
        }
        
        return array();
    }
    
    private function getCmsStatusIdByName($statusName) {
        return self::getStatusConstantList()[strtoupper($statusName)];
    }
    
    private static function getStatusConstantList() {
        //Константы статусов хранятся в purchase-log.class.php, статусы не добавляются в админке
        $purchaseReflection = new ReflectionClass('WPSC_Purchase_Log');
        return $purchaseReflection->getConstants();
    }
    
    private function loadUserSettings() {
        $userSettings = \PaySystem\UserSettings::getInstance();
        
        $params = array();
        foreach ($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            if (in_array($paramName, array('holdMode', 'testMode')) && get_option('intellectMoney_' . $paramName) == 'on') {
                $params[$paramName] = 1;
            } else {
                $params[$paramName] = get_option('intellectMoney_' . $paramName);
            }
        }
        $userSettings->setParams($params);
        $userSettings->setMerchantUrl('https://merchant.intellectmoney.ru');
        return $userSettings;
    }
    
    public static function getAdminForm() {
        $settings = array();
        $languageHelper = \PaySystem\LanguageHelper::getInstance('ru');
        $userSettings = \PaySystem\UserSettings::getInstance();
        foreach($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            $settings[$paramName] = get_option('intellectMoney_' . $paramName);
        }

        $errorsJson = get_option('intellectMoney_settingsSaveErrors');
        $errors = json_decode($errorsJson);

        $html = "";
        foreach($userSettings->getInputTypeForOrganizationParamsToSave() as $inputType => $paramsArray) {
            foreach($paramsArray as $paramName) {
                switch($inputType) {
                    case 'text':
                        $isParamSaveError = array_key_exists($paramName, (array)$errors);
                        $html .= Templates::getAdminInputText($languageHelper->getTitle($paramName), $settings[$paramName], "intellectMoney_" . $paramName, $languageHelper->getDesc($paramName), $isParamSaveError);
                        break;
                    case 'checkbox':
                        $html .= Templates::getAdminInputCheckbox($languageHelper->getTitle($paramName), $settings[$paramName], "intellectMoney_" . $paramName, $languageHelper->getDesc($paramName));
                        break;
                    case 'select':
                        if(stripos($paramName, "status") !== false) {
                            $html .= Templates::getAdminSelector($languageHelper->getTitle($paramName), $settings[$paramName], "intellectMoney_" . $paramName, $languageHelper->getDesc($paramName), wpsc_merchant_intellectmoney::getStatusList());
                        }
                        if(stripos($paramName, "tax") !== false) {
                            $html .= Templates::getAdminSelector($languageHelper->getTitle($paramName), $settings[$paramName], "intellectMoney_" . $paramName, $languageHelper->getDesc($paramName), \PaySystem\VATs::getList());
                        }
                }
            }
        }
        
        $resultUrlAddress = get_option('home') . "/index.php?wpsc_action=gateway_notification&gateway=wpsc_merchant_intellectmoney";
        $html .= Templates::getAdminInputText($languageHelper->getTitle("resultUrl"), $resultUrlAddress, "intellectMoney_resultUrl", $languageHelper->getDesc("resultUrl"), false, true);

        return $html;
    }
    
    public static function saveAdminForm() {
        update_option('intellectMoney_settingsSaveErrors', '');
        $userSettings = \PaySystem\UserSettings::getInstance();

        //Получение
        $saveParams = array();
        foreach($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            $saveParams[$paramName] = $_POST['intellectMoney_' . $paramName];
        }

        //Валидация
        $errors = array();
        $userSettings->setParams($saveParams, $errors);
        if(!empty($errors)) {    
            update_option('intellectMoney_settingsSaveErrors', json_encode($errors));
        }

        //Сохранение
        $validParams = $userSettings->getParams();
        foreach($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            update_option('intellectMoney_' . $paramName, $validParams[$paramName]);
        }
    }
    
    public static function getStatusList() {
        global $wpsc_purchlog_statuses;
        $statusList = array();
        $statusConstantList = self::getStatusConstantList();
        foreach($wpsc_purchlog_statuses as $status) {
            if(array_key_exists(strtoupper($status['internalname']), $statusConstantList)) {
                $statusList[] = array(
                    "id" => $status['internalname'],
                    "name" => $status['label']
                );
            }
        }
        return $statusList;
    }
}

class Templates {   
    private static function getCommonAdminTemplate($displayName, $description, $input) {
        $template = "<tr>
                        <td>%s</td>
                        <td>
                            %s
                            <p class='description'>%s</p>
                        </td>
                    </tr>";
        return sprintf($template, $displayName, $input, $description);
    }
    
    public static function getAdminInputText($displayName, $value, $paramName, $description, $isError = false, $isDisabled = false) {
        $redBorder = $isError ? "style='border: 1px solid red'" : "";
        $disable = $isDisabled ? "disabled style='width: 750px'" : "";
        $input = "<input " . $redBorder . " " . $disable . " type='text' name='" . $paramName . "' value='" . $value . "' />";
        return self::getCommonAdminTemplate($displayName, $description, $input);
    }
    
    public static function getAdminInputCheckbox($displayName, $isChecked, $paramName, $description) {
        $checked = $isChecked ? 'checked' : '';
        $input = "<input type='checkbox' name='" . $paramName . "' " . $checked . " />";
        return self::getCommonAdminTemplate($displayName, $description, $input);
    }
    
    public static function getAdminSelector($displayName, $value, $paramName, $description, $options) {
        $select = "<select style='width: 200px;' name='" . $paramName . "'>";
        foreach($options as $option) {
            $select .= "<option value='" . $option['id'] . "'";
            if($value == $option['id']) {
                $select .= " selected";
            }
            $select .= ">" . $option['name'] . "</option>";
        }
        $select .= "</select>";
        return self::getCommonAdminTemplate($displayName, $description, $select);
    }
}

?>